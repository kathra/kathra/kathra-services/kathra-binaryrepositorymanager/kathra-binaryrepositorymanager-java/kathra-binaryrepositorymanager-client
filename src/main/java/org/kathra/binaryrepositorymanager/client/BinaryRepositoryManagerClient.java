package org.kathra.binaryrepositorymanager.client;

import org.kathra.client.*;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import org.kathra.utils.KathraSessionManager;
import org.kathra.utils.ApiException;
import org.kathra.utils.ApiResponse;

import org.kathra.core.model.BinaryRepository;
import org.kathra.binaryrepositorymanager.model.Credential;
import org.kathra.core.model.Membership;

public class BinaryRepositoryManagerClient {
    private ApiClient apiClient;

    public BinaryRepositoryManagerClient() {
        this.apiClient = new ApiClient().setUserAgent("BinaryRepositoryManagerClient 1.2.0");
    }

    public BinaryRepositoryManagerClient(String serviceUrl) {
        this();
        apiClient.setBasePath(serviceUrl);
    }

    public BinaryRepositoryManagerClient(String serviceUrl, KathraSessionManager sessionManager) {
        this(serviceUrl);
        apiClient.setSessionManager(sessionManager);
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    private void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Build call for addBinaryRepository
     * @param binaryRepository Binary repository object to be created (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call addBinaryRepositoryCall(BinaryRepository binaryRepository, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = binaryRepository;
        
        // create path and map variables
        String localVarPath = "/binaryRepositories/";

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call addBinaryRepositoryValidateBeforeCall(BinaryRepository binaryRepository, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'binaryRepository' is set
        if (binaryRepository == null) {
            throw new ApiException("Missing the required parameter 'binaryRepository' when calling addBinaryRepository(Async)");
        }
        
        
        com.squareup.okhttp.Call call = addBinaryRepositoryCall(binaryRepository, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Add a binary repository
     * 
     * @param binaryRepository Binary repository object to be created (required)
     * @return BinaryRepository
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public BinaryRepository addBinaryRepository(BinaryRepository binaryRepository) throws ApiException {
        ApiResponse<BinaryRepository> resp = addBinaryRepositoryWithHttpInfo(binaryRepository);
        return resp.getData();
    }

    /**
     * Add a binary repository
     * 
     * @param binaryRepository Binary repository object to be created (required)
     * @return ApiResponse&lt;BinaryRepository&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<BinaryRepository> addBinaryRepositoryWithHttpInfo(BinaryRepository binaryRepository) throws ApiException {
        com.squareup.okhttp.Call call = addBinaryRepositoryValidateBeforeCall(binaryRepository, null, null);
        Type localVarReturnType = new TypeToken<BinaryRepository>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Add a binary repository (asynchronously)
     * 
     * @param binaryRepository Binary repository object to be created (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call addBinaryRepositoryAsync(BinaryRepository binaryRepository, final ApiCallback<BinaryRepository> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = addBinaryRepositoryValidateBeforeCall(binaryRepository, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<BinaryRepository>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for addBinaryRepositoryMembership
     * @param binaryRepoId The id of the binary repository to retrieve (required)
     * @param binaryRepositoryMembership Membership object to add to the binary repository (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call addBinaryRepositoryMembershipCall(String binaryRepoId, Membership binaryRepositoryMembership, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = binaryRepositoryMembership;
        
        // create path and map variables
        String localVarPath = "/binaryRepositories/{binaryRepoId}/membership"
            .replaceAll("\\{" + "binaryRepoId" + "\\}", apiClient.escapeString(binaryRepoId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call addBinaryRepositoryMembershipValidateBeforeCall(String binaryRepoId, Membership binaryRepositoryMembership, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'binaryRepoId' is set
        if (binaryRepoId == null) {
            throw new ApiException("Missing the required parameter 'binaryRepoId' when calling addBinaryRepositoryMembership(Async)");
        }
        
        // verify the required parameter 'binaryRepositoryMembership' is set
        if (binaryRepositoryMembership == null) {
            throw new ApiException("Missing the required parameter 'binaryRepositoryMembership' when calling addBinaryRepositoryMembership(Async)");
        }
        
        
        com.squareup.okhttp.Call call = addBinaryRepositoryMembershipCall(binaryRepoId, binaryRepositoryMembership, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Add a binary repository membership for a user or group
     * 
     * @param binaryRepoId The id of the binary repository to retrieve (required)
     * @param binaryRepositoryMembership Membership object to add to the binary repository (required)
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public void addBinaryRepositoryMembership(String binaryRepoId, Membership binaryRepositoryMembership) throws ApiException {
        addBinaryRepositoryMembershipWithHttpInfo(binaryRepoId, binaryRepositoryMembership);
    }

    /**
     * Add a binary repository membership for a user or group
     * 
     * @param binaryRepoId The id of the binary repository to retrieve (required)
     * @param binaryRepositoryMembership Membership object to add to the binary repository (required)
     * @return ApiResponse&lt;Void&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<Void> addBinaryRepositoryMembershipWithHttpInfo(String binaryRepoId, Membership binaryRepositoryMembership) throws ApiException {
        com.squareup.okhttp.Call call = addBinaryRepositoryMembershipValidateBeforeCall(binaryRepoId, binaryRepositoryMembership, null, null);
        return apiClient.execute(call);
    }

    /**
     * Add a binary repository membership for a user or group (asynchronously)
     * 
     * @param binaryRepoId The id of the binary repository to retrieve (required)
     * @param binaryRepositoryMembership Membership object to add to the binary repository (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call addBinaryRepositoryMembershipAsync(String binaryRepoId, Membership binaryRepositoryMembership, final ApiCallback<Void> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = addBinaryRepositoryMembershipValidateBeforeCall(binaryRepoId, binaryRepositoryMembership, progressListener, progressRequestListener);
        apiClient.executeAsync(call, callback);
        return call;
    }
    /**
     * Build call for credentialsIdGet
     * @param id The id of the binary repository to retrieve (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call credentialsIdGetCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/credentials/{id}"
            .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call credentialsIdGetValidateBeforeCall(String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new ApiException("Missing the required parameter 'id' when calling credentialsIdGet(Async)");
        }
        
        
        com.squareup.okhttp.Call call = credentialsIdGetCall(id, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Return credentials for a user
     * getCredentials
     * @param id The id of the binary repository to retrieve (required)
     * @return Credential
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public Credential credentialsIdGet(String id) throws ApiException {
        ApiResponse<Credential> resp = credentialsIdGetWithHttpInfo(id);
        return resp.getData();
    }

    /**
     * Return credentials for a user
     * getCredentials
     * @param id The id of the binary repository to retrieve (required)
     * @return ApiResponse&lt;Credential&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<Credential> credentialsIdGetWithHttpInfo(String id) throws ApiException {
        com.squareup.okhttp.Call call = credentialsIdGetValidateBeforeCall(id, null, null);
        Type localVarReturnType = new TypeToken<Credential>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Return credentials for a user (asynchronously)
     * getCredentials
     * @param id The id of the binary repository to retrieve (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call credentialsIdGetAsync(String id, final ApiCallback<Credential> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = credentialsIdGetValidateBeforeCall(id, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<Credential>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getBinaryRepositories
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getBinaryRepositoriesCall(final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/binaryRepositories/";

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getBinaryRepositoriesValidateBeforeCall(final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        
        com.squareup.okhttp.Call call = getBinaryRepositoriesCall(progressListener, progressRequestListener);
        return call;

    }

    /**
     * Retrieve a list of existing binary repositories for authenticated user
     * 
     * @return List&lt;BinaryRepository&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public List<BinaryRepository> getBinaryRepositories() throws ApiException {
        ApiResponse<List<BinaryRepository>> resp = getBinaryRepositoriesWithHttpInfo();
        return resp.getData();
    }

    /**
     * Retrieve a list of existing binary repositories for authenticated user
     * 
     * @return ApiResponse&lt;List&lt;BinaryRepository&gt;&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<List<BinaryRepository>> getBinaryRepositoriesWithHttpInfo() throws ApiException {
        com.squareup.okhttp.Call call = getBinaryRepositoriesValidateBeforeCall(null, null);
        Type localVarReturnType = new TypeToken<List<BinaryRepository>>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Retrieve a list of existing binary repositories for authenticated user (asynchronously)
     * 
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getBinaryRepositoriesAsync(final ApiCallback<List<BinaryRepository>> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getBinaryRepositoriesValidateBeforeCall(progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<List<BinaryRepository>>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getBinaryRepository
     * @param binaryRepoId The id of the binary repository to retrieve (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getBinaryRepositoryCall(String binaryRepoId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/binaryRepositories/{binaryRepoId}"
            .replaceAll("\\{" + "binaryRepoId" + "\\}", apiClient.escapeString(binaryRepoId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getBinaryRepositoryValidateBeforeCall(String binaryRepoId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'binaryRepoId' is set
        if (binaryRepoId == null) {
            throw new ApiException("Missing the required parameter 'binaryRepoId' when calling getBinaryRepository(Async)");
        }
        
        
        com.squareup.okhttp.Call call = getBinaryRepositoryCall(binaryRepoId, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Retrieve a specific binary repository
     * 
     * @param binaryRepoId The id of the binary repository to retrieve (required)
     * @return BinaryRepository
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public BinaryRepository getBinaryRepository(String binaryRepoId) throws ApiException {
        ApiResponse<BinaryRepository> resp = getBinaryRepositoryWithHttpInfo(binaryRepoId);
        return resp.getData();
    }

    /**
     * Retrieve a specific binary repository
     * 
     * @param binaryRepoId The id of the binary repository to retrieve (required)
     * @return ApiResponse&lt;BinaryRepository&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<BinaryRepository> getBinaryRepositoryWithHttpInfo(String binaryRepoId) throws ApiException {
        com.squareup.okhttp.Call call = getBinaryRepositoryValidateBeforeCall(binaryRepoId, null, null);
        Type localVarReturnType = new TypeToken<BinaryRepository>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Retrieve a specific binary repository (asynchronously)
     * 
     * @param binaryRepoId The id of the binary repository to retrieve (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getBinaryRepositoryAsync(String binaryRepoId, final ApiCallback<BinaryRepository> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getBinaryRepositoryValidateBeforeCall(binaryRepoId, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<BinaryRepository>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getBinaryRepositoryMembership
     * @param binaryRepoId The id of the binary repository to retrieve (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getBinaryRepositoryMembershipCall(String binaryRepoId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/binaryRepositories/{binaryRepoId}/membership"
            .replaceAll("\\{" + "binaryRepoId" + "\\}", apiClient.escapeString(binaryRepoId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getBinaryRepositoryMembershipValidateBeforeCall(String binaryRepoId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'binaryRepoId' is set
        if (binaryRepoId == null) {
            throw new ApiException("Missing the required parameter 'binaryRepoId' when calling getBinaryRepositoryMembership(Async)");
        }
        
        
        com.squareup.okhttp.Call call = getBinaryRepositoryMembershipCall(binaryRepoId, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Retrieve a list of users and groups membership values for the specified binary repository
     * 
     * @param binaryRepoId The id of the binary repository to retrieve (required)
     * @return List&lt;Membership&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public List<Membership> getBinaryRepositoryMembership(String binaryRepoId) throws ApiException {
        ApiResponse<List<Membership>> resp = getBinaryRepositoryMembershipWithHttpInfo(binaryRepoId);
        return resp.getData();
    }

    /**
     * Retrieve a list of users and groups membership values for the specified binary repository
     * 
     * @param binaryRepoId The id of the binary repository to retrieve (required)
     * @return ApiResponse&lt;List&lt;Membership&gt;&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<List<Membership>> getBinaryRepositoryMembershipWithHttpInfo(String binaryRepoId) throws ApiException {
        com.squareup.okhttp.Call call = getBinaryRepositoryMembershipValidateBeforeCall(binaryRepoId, null, null);
        Type localVarReturnType = new TypeToken<List<Membership>>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Retrieve a list of users and groups membership values for the specified binary repository (asynchronously)
     * 
     * @param binaryRepoId The id of the binary repository to retrieve (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getBinaryRepositoryMembershipAsync(String binaryRepoId, final ApiCallback<List<Membership>> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getBinaryRepositoryMembershipValidateBeforeCall(binaryRepoId, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<List<Membership>>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for updateBinaryRepository
     * @param binaryRepoId The id of the binary repository to replace (required)
     * @param binaryRepository Binary repository object to use to replace existing resource (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call updateBinaryRepositoryCall(String binaryRepoId, BinaryRepository binaryRepository, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = binaryRepository;
        
        // create path and map variables
        String localVarPath = "/binaryRepositories/{binaryRepoId}"
            .replaceAll("\\{" + "binaryRepoId" + "\\}", apiClient.escapeString(binaryRepoId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "PUT", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call updateBinaryRepositoryValidateBeforeCall(String binaryRepoId, BinaryRepository binaryRepository, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'binaryRepoId' is set
        if (binaryRepoId == null) {
            throw new ApiException("Missing the required parameter 'binaryRepoId' when calling updateBinaryRepository(Async)");
        }
        
        // verify the required parameter 'binaryRepository' is set
        if (binaryRepository == null) {
            throw new ApiException("Missing the required parameter 'binaryRepository' when calling updateBinaryRepository(Async)");
        }
        
        
        com.squareup.okhttp.Call call = updateBinaryRepositoryCall(binaryRepoId, binaryRepository, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Fully update a registered binary repository
     * 
     * @param binaryRepoId The id of the binary repository to replace (required)
     * @param binaryRepository Binary repository object to use to replace existing resource (required)
     * @return BinaryRepository
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public BinaryRepository updateBinaryRepository(String binaryRepoId, BinaryRepository binaryRepository) throws ApiException {
        ApiResponse<BinaryRepository> resp = updateBinaryRepositoryWithHttpInfo(binaryRepoId, binaryRepository);
        return resp.getData();
    }

    /**
     * Fully update a registered binary repository
     * 
     * @param binaryRepoId The id of the binary repository to replace (required)
     * @param binaryRepository Binary repository object to use to replace existing resource (required)
     * @return ApiResponse&lt;BinaryRepository&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<BinaryRepository> updateBinaryRepositoryWithHttpInfo(String binaryRepoId, BinaryRepository binaryRepository) throws ApiException {
        com.squareup.okhttp.Call call = updateBinaryRepositoryValidateBeforeCall(binaryRepoId, binaryRepository, null, null);
        Type localVarReturnType = new TypeToken<BinaryRepository>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Fully update a registered binary repository (asynchronously)
     * 
     * @param binaryRepoId The id of the binary repository to replace (required)
     * @param binaryRepository Binary repository object to use to replace existing resource (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call updateBinaryRepositoryAsync(String binaryRepoId, BinaryRepository binaryRepository, final ApiCallback<BinaryRepository> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = updateBinaryRepositoryValidateBeforeCall(binaryRepoId, binaryRepository, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<BinaryRepository>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for updateBinaryRepositoryAttributes
     * @param binaryRepoId The id of the binary repository to partially update (required)
     * @param binaryRepository Binary repository object to use to patch existing resource (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call updateBinaryRepositoryAttributesCall(String binaryRepoId, BinaryRepository binaryRepository, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = binaryRepository;
        
        // create path and map variables
        String localVarPath = "/binaryRepositories/{binaryRepoId}"
            .replaceAll("\\{" + "binaryRepoId" + "\\}", apiClient.escapeString(binaryRepoId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "PATCH", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call updateBinaryRepositoryAttributesValidateBeforeCall(String binaryRepoId, BinaryRepository binaryRepository, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'binaryRepoId' is set
        if (binaryRepoId == null) {
            throw new ApiException("Missing the required parameter 'binaryRepoId' when calling updateBinaryRepositoryAttributes(Async)");
        }
        
        // verify the required parameter 'binaryRepository' is set
        if (binaryRepository == null) {
            throw new ApiException("Missing the required parameter 'binaryRepository' when calling updateBinaryRepositoryAttributes(Async)");
        }
        
        
        com.squareup.okhttp.Call call = updateBinaryRepositoryAttributesCall(binaryRepoId, binaryRepository, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Partially update a registered binary repository
     * 
     * @param binaryRepoId The id of the binary repository to partially update (required)
     * @param binaryRepository Binary repository object to use to patch existing resource (required)
     * @return BinaryRepository
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public BinaryRepository updateBinaryRepositoryAttributes(String binaryRepoId, BinaryRepository binaryRepository) throws ApiException {
        ApiResponse<BinaryRepository> resp = updateBinaryRepositoryAttributesWithHttpInfo(binaryRepoId, binaryRepository);
        return resp.getData();
    }

    /**
     * Partially update a registered binary repository
     * 
     * @param binaryRepoId The id of the binary repository to partially update (required)
     * @param binaryRepository Binary repository object to use to patch existing resource (required)
     * @return ApiResponse&lt;BinaryRepository&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<BinaryRepository> updateBinaryRepositoryAttributesWithHttpInfo(String binaryRepoId, BinaryRepository binaryRepository) throws ApiException {
        com.squareup.okhttp.Call call = updateBinaryRepositoryAttributesValidateBeforeCall(binaryRepoId, binaryRepository, null, null);
        Type localVarReturnType = new TypeToken<BinaryRepository>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Partially update a registered binary repository (asynchronously)
     * 
     * @param binaryRepoId The id of the binary repository to partially update (required)
     * @param binaryRepository Binary repository object to use to patch existing resource (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call updateBinaryRepositoryAttributesAsync(String binaryRepoId, BinaryRepository binaryRepository, final ApiCallback<BinaryRepository> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = updateBinaryRepositoryAttributesValidateBeforeCall(binaryRepoId, binaryRepository, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<BinaryRepository>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
